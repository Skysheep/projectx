﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasisAction : MonoBehaviour {

    CharacterController controller;
    Animator animator;
    SpriteRenderer spriteRenderer;


    public float WalkSpeed = 1.0f;
    public float JumpSpeed = 3.0f;

    private Vector2 moveDirection = Vector2.zero;

	// Animator Parameter
    private float Speed, FallSpeed, x;
    private bool IsOnGround, IsDamage, IsDead;
    public Vector3 gravity;

    void Awake(){
        controller = GetComponent<CharacterController> ();
        animator = GetComponent<Animator> ();
        spriteRenderer = GetComponent<SpriteRenderer> ();
        gravity = new Vector3(0.0f,1f,0.0f);
        Debug.Log(gravity);
    }

    void Update(){

        IsOnGround = controller.isGrounded;

        x = Input.GetAxis("Horizontal");
        moveDirection = new Vector2(x*WalkSpeed, 0);

        if (IsOnGround) {
            Debug.Log("OnGround");

            if (Input.GetButton("Jump")){
                moveDirection.y = JumpSpeed;
            }
        } else {
            moveDirection.y -= gravity.y * Time.deltaTime;
        }
        controller.Move(moveDirection * Time.deltaTime);

        animator.SetFloat("Speed",controller.velocity.x);
        animator.SetFloat("FallSpeed",controller.velocity.y);
        animator.SetBool("IsOnGround", IsOnGround);

        // flip sprite
        if (x != 0){
            spriteRenderer.flipX = x < 0;
        }
    }
}
