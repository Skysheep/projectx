﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Meau : UI_Layer {


	public override void OnButtonClick(string name, GameObject obj){
		switch(name){
			case "Continue":
				UIManager.ExitUI(this);
				break;

			case "DebugMode":

				break;

			case "Setting":

				break;

			case "MainMeau":
				SceneController.LoadScene("Start");
				UIManager.ExitALLUI();
				break;
		}
	}

}
