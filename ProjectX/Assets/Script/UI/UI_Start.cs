﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class UI_Start : UI_Layer {
	public GameObject PressAnyKey, Button;
	public Text mTextPressAnyKey;

	// Use this for initialization

	public void Start(){
		this.PressAnyKey.SetActive(true);
		this.Button.SetActive(false);

	}

	// Update is called once per frame
	void Update () {

		if(Input.anyKeyDown){
    		this.PressAnyKey.SetActive(false);
			this.Button.SetActive(true);
    	}
	}



	public override void OnNodeAsset(string name, GameObject obj){
		switch(name){
			case "PressAnyKey":
				this.PressAnyKey = obj;
				this.mTextPressAnyKey = obj.GetComponent<Text>();
				break;

			case "Button":
				this.Button = obj;
				break;

		}
	}

	public override void OnButtonClick(string name, GameObject obj){
		switch(name){
			case "NewGame":
				SceneController.LoadScene("Level1-1");
				break;

			case "LoadGame":

				break;

			case "Setting":

				break;

			case "Exit":
				Application.Quit();
				break;
		}
	}
}
