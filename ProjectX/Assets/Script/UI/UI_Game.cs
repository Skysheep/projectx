﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Game : UI_Layer {


	public override void OnButtonClick(string name, GameObject obj){
		switch(name){
			case "Meau":
				Debug.Log("EnterUI<UI_Meau>");
				UIManager.EnterUI<UI_Meau>();
				break;
		}
	}

	/*
	GameObject[] respawns = GameObject.FindGameObjectsWithTag("respawn");

	GameObject player;

	void Start(){
		player = FindObjectOfType(typeof(Player));
	}
	*/

}


/*
開啟DebugMose:
當我創建UI_Meau之後，會出現開關DebugMode的選項，
若此時我將UI_Meau Destory()，則會把DebugMode的開關物件一併。
有兩個方式解決
1. 將 bool IsDebugOn 存入  PlayerPrefs.SetInt("IsDebugOn", 0)，再次讀取時要先存取PlayerPrefs
2. 將 UI_Meau inactive ，並儲存物件路徑，然後再打開。
*/

/*
尋找Scnen 中的 Object :
MyScript[] myScriptObjects =
FindObjectsOfType(typeof(MyScript)) as MyScript[];

or

	//測試中的///
	GameObject player;

	void Start(){
		player = FindObjectOfType(typeof(Player));
	}
	//測試中的///


*/