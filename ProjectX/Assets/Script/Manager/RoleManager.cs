﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoleManager {
	public const string CharPath = "Character/Prefabs";
	public const string MobPath = "Mob/Prefabs";

	public static GameObject CreatePlayer(string name){
		GameObject obj =  Resources.Load<GameObject>(CharPath + name);
		GameObject Player = GameObject.Instantiate(obj);
		return Player;
	}
}
