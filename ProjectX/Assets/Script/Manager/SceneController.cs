using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

	[SceneName]
    public string nextScene;

    /*
    public bool swichSceneDirectly;
    [SerializeField]
    private KeyCode pressKeySwitch = KeyCode.None;
    */

    /*
    public  SceneMode mode;
		public enum SceneMode {
			Single,
			Additive
    	}
        public string SceneModeName = string.Concat("LoadSceneMode.", mode);
    */

    /*
    void Update(){
    	if(Input.GetKeyDown(pressKeySwitch) || swichSceneDirectly == true ){
    		StartCoroutine(loadScene());
    	}
    }
    */

    public static void LoadScene(string name){

            // Debug.Log(SceneModeName);
    		// SceneManager.LoadScene(nextScene, SceneModeName);
            SceneManager.LoadScene(name, LoadSceneMode.Single);

    }
}


/*
    public int editNum;
    public Color color;
    public Vector3 position;
    public bool check;

    public enum customExpand {choose1, choose2 }
    public customExpand expand;
*/