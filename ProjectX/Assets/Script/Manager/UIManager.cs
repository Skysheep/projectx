﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager {

	public const string UIPath = "UIPrefabs/";
	public static GameObject mUIRoot;
	public static List<MonoBehaviour> mUIList = new List<MonoBehaviour>();

	private static void InitData() {
		mUIRoot = GameObject.Find("Canvas");
	}

// 	||========================================||
// 	||open UI: UIManager.EnterUI<UI_name>(); ||
// 	||========================================||
	public static T EnterUI<T>() where T : UI_Layer {

		if(mUIRoot == null){
			InitData();
		}

		GameObject obj = Resources.Load<GameObject>(UIPath + typeof(T).ToString());
		GameObject ui = GameObject.Instantiate(obj);
		ui.transform.SetParent(mUIRoot.transform);
		ui.transform.localPosition = Vector3.zero;
		ui.transform.localScale = Vector2.one;
		ui.transform.localRotation = Quaternion.identity;

		RectTransform rect = ui.GetComponent<RectTransform>();
		rect.offsetMax = Vector2.zero;
		rect.offsetMin = Vector2.zero;

		T t = ui.AddComponent<T>();
		t.__init_node(t.transform);
		mUIList.Add(t);
		return t;
	}


// 	||==================================||
// 	||close UI: UIManager.ExitUI(this); ||
// 	||==================================||
	public static void ExitUI(MonoBehaviour mono){
		mUIList.Remove(mono);
		GameObject.Destroy(mono.gameObject);
	}

	public static void ExitALLUI(){
		for(int i = 0; i < mUIList.Count; i++){
			GameObject.Destroy(mUIList[i].gameObject);
		}
		mUIList.Clear();
	}
}
