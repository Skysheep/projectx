﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Layer : MonoBehaviour {

	public void __init_node(Transform tf) {

		this.OnNodeAsset(tf.name, tf.gameObject);
		Button btn = tf.GetComponent<Button>();
		if (btn != null){
			btn.onClick.AddListener(() => {
				this.OnButtonClick(btn.name, btn.gameObject);
				Debug.Log("init"+ tf);
			});
		}
		for(int i = 0; i < tf.childCount; i++){
			this.__init_node(tf.GetChild(i));
		}
	}



	public virtual void OnLoad(){

	}


	public virtual void OnNodeAsset(string name, GameObject obj){

	}

	public virtual void OnButtonClick(string name, GameObject obj){

	}
}
