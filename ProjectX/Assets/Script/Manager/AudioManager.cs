using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager {

	public const string BGMPath = "Audio/BGM";
	public const string SEPath = "Audio/SE";
	// public const string VoicePath = "Audio/Voice";

	public static bool mIsBGMOn, mIsSEOn; // 設定音效開關

	public static void SetBGMOn(bool is_on){
		mIsBGMOn = is_on;
		if(is_on){ PlayerPrefs.SetInt("is_BGM_on", 1); }
		else { PlayerPrefs.SetInt("is_BGM_on", 0); } // 將音效設定儲存至PlayerPrefs

		if(_as_BGM != null){
			if(_as_BGM.isPlaying) { _as_BGM.Stop(); }
			else { _as_BGM.Play(); }
		}
	}

	public static void SetSEOn(bool is_on){
		mIsSEOn = is_on;
		if(is_on){ PlayerPrefs.SetInt("is_SE_on", 1); }
		else { PlayerPrefs.SetInt("is_SE_on", 0); }

		for(int i = 0; i < _as_SE_List.Count; i++ ){
			if(_as_SE_List[i].isPlaying && is_on == false){
				_as_SE_List[i].Stop();
			}
		}

	}

	// 這個未來要搬出去 //
	public static void InitData(){
		mIsBGMOn = PlayerPrefs.GetInt("is_BGM_on", 1) == 1;
		mIsSEOn = PlayerPrefs.GetInt("is_SE_on", 1) == 1;
	}


//	====================================
//  || 播放BGM: PlayBGM(string name); ||
//  ====================================
	public static AudioSource _as_BGM = null;

	public static void PlayBGM(string name){
		if(_as_BGM == null){
			_as_BGM = Camera.main.gameObject.AddComponent<AudioSource>();
		}

		_as_BGM.clip = Resources.Load<AudioClip>(BGMPath + name);
		_as_BGM.loop = true;
		if(mIsBGMOn){ _as_BGM.Play(); }
	}


//	==================================
//  || 播放SE: PlaySE(string name); ||
//  ==================================
	public static List<AudioSource> _as_SE_List = new List<AudioSource>();

	public static void PlaySE(string name){
		if(mIsSEOn == false){ return; }
		AudioSource _as_SE = null;
		for(int i = 0; i < _as_SE_List.Count; i++){
			if(_as_SE_List[i].isPlaying == false){
				_as_SE = _as_SE_List[i];
				break;
			}
		}

		if(_as_SE == null){
			_as_SE = Camera.main.gameObject.AddComponent<AudioSource>();
			_as_SE_List.Add(_as_SE);
		}
		_as_SE.clip = Resources.Load<AudioClip>(SEPath + name);
		_as_SE.loop = false;
		if(mIsSEOn){ _as_BGM.Play(); }
	}
}
