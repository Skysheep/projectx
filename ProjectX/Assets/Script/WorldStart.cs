﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldStart : World {


	// Use this for initialization
	void Start () {

		AudioManager.InitData();
		UIManager.EnterUI<UI_Start>();

	}


}
