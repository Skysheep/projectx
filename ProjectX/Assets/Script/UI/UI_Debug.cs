﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Debug : UI_Layer {
	public Text mBelow, mAbove, mLeft, mRight, mSlope, mWallSliding, mIsDamage;

	public Controller2D controller;
	public Player player;

	// Use this for initialization

	public void Awake(){
		__init_node(this.transform);
		player = player.GetComponent<Player> ();
		controller = controller.GetComponent<Controller2D> ();

		Debug.Log("Setup");
	}

	// Update is called once per frame
	void Update () {


		this.mAbove.text = "Above:" + BoolToString(controller.collisions.above) + controller.collisions.abovetag;
		this.mBelow.text = "Below:" + BoolToString(controller.collisions.below) + controller.collisions.belowtag;
		this.mLeft.text = "Left:" + BoolToString(controller.collisions.left) + controller.collisions.lefttag;
		this.mRight.text = "Right:" + BoolToString(controller.collisions.right) + controller.collisions.righttag;
		this.mSlope.text = "Slope:" + controller.collisions.slopeAngle;
		this.mWallSliding.text = "WallSliding:" + BoolToString(player.wallSliding);
		this.mIsDamage.text = "IsDamage:" + BoolToString(player.IsDamage);

	}



	public override void OnNodeAsset(string name, GameObject obj){
		switch(name){
			case "Above":
				this.mAbove = obj.GetComponent<Text>();
				break;

			case "Below":
				this.mBelow = obj.GetComponent<Text>();
				break;

			case "Left":
				this.mLeft = obj.GetComponent<Text>();
				break;

			case "Right":
				this.mRight = obj.GetComponent<Text>();
				break;

			case "Slope":
				this.mSlope = obj.GetComponent<Text>();
				break;

			case "WallSliding":
				this.mWallSliding = obj.GetComponent<Text>();
				break;

			case "IsDamage":
				this.mIsDamage = obj.GetComponent<Text>();
				break;

		}
	}

	static string BoolToString(bool b) {
 		return b ? "True":"False";
 	}
}
